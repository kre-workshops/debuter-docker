# Docker Compose

Un des principes fondamentaux des conteneurs est d'isoler un processus et Docker suit ce principe. Ce qui veut dire que **NORMALEMENT** un conteneur doit faire tourner un seul processus.

Sur ce principe si je veux faire fonctionner un wordpress et séparer la partie code/web et la partie sql il me faut déjà deux conteneurs.


## A la main

On commence par instancier le conteneur sql :

    # docker container run --name sql01 -e MYSQL_ROOT_PASSWORD=secret -d mariadb

J'en profites pour introduire ici les variables d'environnement, suivant les images on va pouvoir envoyer différents paramètres en ligne de commande, souvant ces variables sont en majuscule.  
On retrouve la définition de ces variables dans la documentation des images.

Une fois le conteneur sql en place on va pouvoir instancier le conteneur wordpress :

    # docker container  run --name wp01 --link sql01:mysql -p 80:80 -d wordpress

Si tout se passe bien quand on va dans le navigateur sur l'ip de la machine on arrive sur la configuration de wordpress et tout se configure correctement.

Vérifiez que la base de donnée est bien enregistrée dans le conteneur sql.

J'ai introduit aussi ici la notion de **link** ou on le verra ensuite la **dépendance** d'un conteneur vis à vis d'un autre.

On dis ici que le conteneur wordpress est lié au conteneur sql01 et que dans le conteneur wp01 pour se connecter au conteneur sql01 il faudra l'appeler mysql. En fait il existe un système interne de gestion dns.

Si vous allez dans le conteneur wp01 et faite un ping sur mysql :

    # ping mysql
    PING mysql (172.17.0.3): 56 data bytes
    64 bytes from 172.17.0.3: icmp_seq=0 ttl=64 time=0.072 ms

Il ping l'ip du conteneur mariadb. Très pratique en plus si le conteneur change d'ip, ce qui arrive si on l'arrête ou on le recrée, le link se refera sur la nouvelle adresse IP.

Dans al conf de wordpress on défini la configuration vers le mariadb avec mysql.

Imaginez maintenant que l'on rajoute un serveur redis, du php en mode fpm, etc, il faudra rajouter autant de conteneur, les linker à la main et démarrer tout ça.  
Il faudra aussi tout redémarrer à la main si on a besoins d'arrêter ou de dupliquer des configurations.

Pas très pratique.

## Docker Compose

![Docker Compose](../images/docker_compose.jpeg)

Heureusement les développeurs de Docker ont inventé Docker Compose.  
Docker Compose est un outils extérieur au moteur docker, il faut donc l'installer avant de l'utiliser en suivant le [guide](https://docs.docker.com/compose/install/)

Une fois en place on se retrouve avec une commande docker-compose.

Il faut dans un premier temps créer un répertoire et dans celui-ci créer un fichier nommé docker-compose.yml.

Ce fichier est au format [YAML](https://fr.wikipedia.org/wiki/YAML) il va définir toute la composition de notre stack de conteneurs.  
Attention le langage YAML est très exigeant dans les indentations et le formalisme.

Prenons l'exemple classique d'une application de vote qui donne le choix entre les chiens et les chats :

	version: "3"
     
	services:
     
	  redis:
	    image: redis:3.2-alpine
	    ports:
	      - "6379"
	    networks:
	      - voteapp
     
	  db:
	    image: postgres:9.4
	    volumes:
	      - db-data:/var/lib/postgresql/data
	    networks:
	      - voteapp
     
	  voting-app:
	    image: gaiadocker/example-voting-app-vote:good
	    ports:
	      - 5000:80
	    networks:
	      - voteapp
	    depends_on:
	      - redis
     
	  result-app:
	    image: gaiadocker/example-voting-app-result:latest
	    ports:
	      - 5001:80
	    networks:
	      - voteapp
	    depends_on:
	      - db
     
	  worker:
	    image: gaiadocker/example-voting-app-worker:latest
	    networks:
	      voteapp:
	        aliases:
	          - workers
	    depends_on:
	      - db
	      - redis
     
	networks:
	    voteapp:
     
	volumes:
	  db-data:

On distingue 3 parties :
* les services
* les réseaux
* les volumes

Par exemple :

    voting-app:
	  image: gaiadocker/example-voting-app-vote:good
	  ports:
	    - 5000:80
	  networks:
	    - voteapp
	  depends_on:
	    - redis

On peut rajouter pour chaque services toutes les options que l'on précise sur la ligne de commande de docker container.

Ici le service est l'application de vote, on définit :
* l'image à utiliser
* le binding des ports
* les réseaux à utiliser (ici crée un réseau spécifique pour l'intercommunication entre les services)
* les dépendances

De la même façon pour la base de données :

	db:
	  image: postgres:9.4
	  volumes:
	    - db-data:/var/lib/postgresql/data
	  networks:
	    - voteapp


Il ne faut pas oublier à la fin de lister les réseaux et les volumes :

	networks:
	    voteapp:
     
	volumes:
	  db-data:


## Les commandes

Toutes les commandes doivent être exécutées dans le répertoire dans lequel se trouve le fichier docker-compose.yml.

Il peut être utile de récupérer les images utilisées avant de déployer la stack :

    # docker-compose pull

On peut maintenant démarrer notre stack :

	# docker-compose up -d
	Creating network "votingapp_voteapp" with the default driver
	Creating volume "votingapp_db-data" with default driver
	Creating votingapp_redis_1 ... 
	Creating votingapp_db_1 ... 
	Creating votingapp_redis_1
	Creating votingapp_db_1 ... done
	Creating votingapp_result-app_1 ... 
	Creating votingapp_redis_1 ... done
	Creating votingapp_voting-app_1 ... 
	Creating votingapp_worker_1 ... 
	Creating votingapp_worker_1
	Creating votingapp_voting-app_1 ... done

On peut lister les conteneurs :

	# docker container ls
	CONTAINER ID        IMAGE                                         COMMAND                  CREATED             STATUS              PORTS                     NAMES
	c903aef6b8a9        gaiadocker/example-voting-app-vote:good       "gunicorn app:app ..."   16 minutes ago      Up 16 minutes       0.0.0.0:5000->80/tcp      votingapp_voting-app_1
	a4f9f096055b        gaiadocker/example-voting-app-worker:latest   "/bin/sh -c 'dotne..."   16 minutes ago      Up 16 minutes                                 votingapp_worker_1
	a547d8a7232d        gaiadocker/example-voting-app-result:latest   "node server.js"         16 minutes ago      Up 16 minutes       0.0.0.0:5001->80/tcp      votingapp_result-app_1
	e8e3f7f4fdac        postgres:9.4                                  "docker-entrypoint..."   16 minutes ago      Up 16 minutes       5432/tcp                  votingapp_db_1
	61b6ef958827        redis:3.2-alpine                              "docker-entrypoint..."   16 minutes ago      Up 16 minutes       0.0.0.0:32768->6379/tcp   votingapp_redis_1


Ou juste les services de la stack :

	# docker-compose ps
	         Name                       Command               State            Ports         
	-----------------------------------------------------------------------------------------
	votingapp_db_1           docker-entrypoint.sh postgres    Up      5432/tcp               
	votingapp_redis_1        docker-entrypoint.sh redis ...   Up      0.0.0.0:32769->6379/tcp
	votingapp_result-app_1   node server.js                   Up      0.0.0.0:5001->80/tcp   
	votingapp_voting-app_1   gunicorn app:app -b 0.0.0. ...   Up      0.0.0.0:5000->80/tcp   
	votingapp_worker_1       /bin/sh -c dotnet Worker.dll     Up                             


On peut voir les logs de l'ensemble :

    # docker-compose logs

Ou les logs d'un service :

    # docker-compose logs db

On peut arrêter toute la stack :

	# docker-compose stop
	Stopping votingapp_voting-app_1 ... done
	Stopping votingapp_worker_1     ... done
	Stopping votingapp_result-app_1 ... done
	Stopping votingapp_db_1         ... done
	Stopping votingapp_redis_1      ... done

On peut tout supprimer d'un coup :

	# docker-compose rm
	Going to remove votingapp_voting-app_1, votingapp_worker_1, votingapp_result-app_1, votingapp_db_1, votingapp_redis_1
	Are you sure? [yN] y
	Removing votingapp_voting-app_1 ... done
	Removing votingapp_worker_1     ... done
	Removing votingapp_result-app_1 ... done
	Removing votingapp_db_1         ... done
	Removing votingapp_redis_1      ... done
