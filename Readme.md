# Workshop de découverte Docker

![Logo Docker](./images/docker.png)


## Bienvenue

**Apprenez à utiliser Docker comme un atout dans votre travail.**

L'objectif de ce Workshop est de vous donner par la pratique les bonnes bases pour l'utilisation de Docker.


## Merci à nos partenaires

![Nos partenaires](./images/partenaires.png)

Pour le prêt du local et la communication : [Loco Numérique](http://loco-numerique.fr/)

Pour le café et le petit déjeuné : [Code 2 Be](http://code2.be/)

Pour la fourniture du réseau : [Ozérim](https://www.ozerim.com/)

Pour le repas du midi : [Hisyl](https://www.hisyl.fr/)


## K' Rément Libre

Association Vendéeen de promotion et de défense du logiciel libre, créée en 2017.

Différents événements dans l'année : conférences, ateliers, workshops, install party, projection de documentaires.

_Nous suivre_

Twitter : @krementlibre

Facebook : Page @krementlibre

Site : www.krementlibre.org


## Votre coach

![Guillaume CHÉRAMY](./images/guillaume.png)

Fervent défenseur des logiciels libre depuis **1998**

*Co-Organisateur* des _Journées du Libre en Touraine_ en 2006 et 2007

*Patron* d'une entreprise d'hébergement et d'infogérence libre depuis 2015: Hisyl

*Président* de l'association _K' Rément Libre_ 

*Professeur* en écoles d'ingénieurs 


## Programme

**8h30**  
**Accueil**  
Un petit café et des viennoiseries pour vous accueillir.


**09h00 - 10h30**  
**Installation de Docker**  
Installation de Docker et première commande pour valider le fonctionnement.


**10h45 - 12h30**  
**Utilisation de base**  
Gros chapitre dans lequel nous verrons les commandes de :
* gestion des conteneurs
* gestion des images
* les volumes
* les ports réseau


**Pause déjeuné**  
Une petite pause pour reprendre des forces


**14h00 - 15h00**  
**Gérer des conteneurs avec une interface**  
Et oui Docker ce n'est pas que des lignes de commandes. Des outils existent pour faciliter la gestion de vos conteneurs, graphiquement.


**15H00 - 16H30**  
**Docker Compose**  
Docker Compose est un outil de mise en place d'applications multi-conteneurs. Vous apprendrez ici à l'utiliser.


**17h00**  
**Fin**  
Bilan de la journée.
