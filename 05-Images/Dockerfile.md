#Créer des images Docker

##Prérequis

Docker installé et fonctionnel
Créer un répertoire pour l'application
Avoir un peu de code :-)

## Code de l'application

	const http = require('http');
	
	const hostname = '0.0.0.0';
	const port = 80;
	
	const server = http.createServer((req, res) => {
	    res.statusCode = 200;
	      res.setHeader('Content-Type', 'text/plain');
	        res.end('Hello World\n');
	});
	
	server.listen(port, hostname, () => {
	    console.log('Server running at http://%s:%s/', hostname, port);
	});
	
	process.on('SIGINT', function() {
	    console.log('Caught interrupt signal and will exit');
	    process.exit();
	});

Cette application nodejs affiche le message "Hello World"

## Dockerfile

Le Dockerfile est la "recette" de création de notre image. Il doit être dans le même fichier que l'application.

Première instrcution, définir à partir de quelle image nous allons créer la notre :

	FROM node:6

Pour rappel, documentez vous sur les images utilisées :
[Node Image](https://hub.docker.com/_/node/) https://hub.docker.com/_/node/

On va ensuite préciser le responsable de l'image :

	MAINTAINER Guillaume Chéramy <guillaume.cheramy@hisyl.fr>
	
Pour cette application on va définir un répertoire de travail /app sur le conteneur :

	WORKDIR /app
	
On recopie l'application dans le répertoire de travail :

	ADD ./app.js /app
	
L'application devra être disponible sur le port 80 :

	EXPOSE 80
	
Pour finir, il faut que l'application fonctionne en continue dans le conteneur :

	CMD ["node", "app.js"]
	
Le Dockerfile final :

	FROM node:6
	
	MAINTAINER Guillaume Chéramy <guillaume.cheramy@hisyl.fr>
	
	WORKDIR /app
	
	ADD ./app.js /app
	
	EXPOSE 80
	
	CMD ["node", "app.js"]
	
## Créer l'image

Une fois notre image décrite dans le Dockerfile, on va pouvoir la créer :

	# docker build -t node-app:0.1 .
	
Attention au point final essentiel.

Le 0.1 représente le tag de l'image, essentiel si on veut maintenir des suivi de version de l'application.

Notre image est maintenant prête :

	# docker image ls
	REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
node-app            0.1                 4be6775ab42f        23 seconds ago      662MB

## Créer un conteneur avec cette image

On peut maintenant instancier notre conteneur avec cette image :

	$ docker run -p 4000:80 --name my-app -d node-app:0.1
	$ docker ps
	CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                  NAMES
	36d6cb42d7a8        node-app:0.1        "node app.js"       3 seconds ago       Up 2 seconds        0.0.0.0:4000->80/tcp   my-app
	
Vérifiez dans votre navigateur ou en local avec curl :
	curl http://localhost:4000


## Modifier une image

Si on met à jour le code, il faut recréer une image.

On modifie par exemple la ligne :
	res.end('Hello World\n');

En 

	res.end('Hello la Loco, vive Docker\n');
	
Docker maintient un cache donc la recréation de l'image sera plus rapide. Ne pas oublier de changer le tag :

	docker build -t node-app:0.2 .
	
On peut alors réinstancier un conteneur avec cette nouvelle image.

	


	


