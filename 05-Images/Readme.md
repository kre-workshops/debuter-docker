# Création d'images Docker

Nous allons voir dans ce chapitre comment créer des images pour Docker et comment les stocker sur le hub.

Première méthode pour créer des images :

[Modifier une image existante](https://framagit.org/kre-workshops/debuter-docker/blob/master/05-Images/Modifier_Image_Existante.md) 

Nous verrons ensuite comment créer un Dockerfile pour une petite application en nodejs.

[Créer une image ](https://framagit.org/kre-workshops/debuter-docker/blob/master/05-Images/Dockerfile.md) 

Puis nous verrons comment créer une image de service (Apache) et comment la pousser sur le hub docker.

[Créer une image suite](https://framagit.org/kre-workshops/debuter-docker/blob/master/05-Images/Dockerfile-suite.md) 