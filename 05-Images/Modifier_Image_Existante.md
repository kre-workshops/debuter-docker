# Modifier une image existante

Le premier réflex pour créer une image est de modifier une image existante. Cela peut se faire en déployant un conteneur à partir d'une image et en modifiant dans le conteneur.

## Fonctionnement d'une image
Une image se compose d'un ensemble de couches superposées les unes aux autres, chaque couche est issue d'une modification de l'image d'origine.

Quand on déploie un conteneur, le système de fichier est présenté au conteneur avec la concaténation des système de fichier des layers de l'image. C'est l'**unionfs**.

Les couches sont en lecture seule, seule la dernière couche créée lors de l'instanciation du conteneur est en écriture ... attention si le conteneur est supprimé cette couche est aussi supprimée.

Si un fichier d'une couche précédente doit être modifié, il sera remonté sur la dernière couche pour petre modifié, c'est le **Copie On Write**.

Un petit schéma : 

![layer d'image](./layers_images.png  "layer d'image")

## Modifier une image
Démarrons un conteneur Debian :

     # docker run --name web0 -it -p 8080:80 debian:9.3-slim
	
On va y installer quelques paquets :

     # apt-get update && apt-get install htop apache2 
     # service apache2 start
	
Si on venut fixer les modifications pour créer une image à partir de cet état. On sort du conteneur et :

     # docker commit web0 debian_guillaume
	
A partir de là on peut ré instancier un conteneur à partir de l'image :

     # docker run --name web01 -it debian_guillaume
	
	
C'est une méthode mais pas la plus conventionnelle.



