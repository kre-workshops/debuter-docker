# Continuons avec le Dockerfile

Cette fois-ci nous allons créer une image apache avec un volume pour le répertoire /var/www.

# Le Dockerfile

	FROM debian:9.3-slim
	MAINTAINER Guillaume Chéramy <guillaume.cheramy@hisyl.fr>
	
	# Install Apache2
	RUN apt update && \
	apt install -y apache2 && \
	apt clean
	
	# Set log directory
	ENV APACHE_LOG_DIR /var/log/apache2
	
	EXPOSE 80
	
	# Run Apache2
	ENTRYPOINT ["/usr/bin/apache2ctl","-D","FOREGROUND"]
	
Avec cet exemple, on peut voir qu'il est possible de faire installer des paquets avec **RUN**.
Attention chaque ligne ligne va créer une couche dans l'image.

On peut rajouter des variables d'environnement avec **ENV**.

Petite différence entre CMD et ENTRYPOINT.

Avec ENTRYPOINT il est possible de rentrer des arguments pour la commande spécifiée alors que cela n'est pas possible avec CMD.

# Utiliser le hub docker pour stocker les images

Première étape créer un compte.

Ensuite se connecter :

	# docker login
	Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
	Username: guidtz
	Password: 
	Login Succeeded
	
Changer le nom de l'image qui doit être préfixée par le login sur le hub :

	# docker tag debian-apache-perso:0.1 guidtz/debian-apache-perso:0.1
	
Reste plus qu'à pousser l'image sur le hub :

	# docker push guidtz/debian-apache-perso:0.1
	
	
