# Débuter

## Docker ?

[Quoi de Neuf Docker 101](https://www.youtube.com/watch?v=zO4-1D9LdDw&index=10&list=PLsEeUqX8gm0N1Giw5Rsq09FmpZ5K4zsz2)

## Les versions de Docker

Jusqu'à Mars 2017 les versions étaient estempillées 0.1 0.2 ... 1.13 

Depuis Mars 2017 un nouveau système de numération à été mis en place et deux versions différentes sont disponibles.

**Docker Community Edition (CE)**
* gratuite
* release stable tout les 3 mois
* release edge tous les mois

**Docker Entreprise Edition (EE)**
* 3 versions : basic, standard, avancé
* release tout les 3 mois avec support pendant 1 ans
* plateforme CaaS (Container As A Service)
* infrastructures certifiées avec support avancé
* version en place dans Windows 2016


![Releases Docker](../images/docker_ce_ee_release.jpg)

Sauf pour ceux installant Docker sous Windows 2016, on utilisera la version Docker CE.

## Installation

Plusieurs choix possibles :

**Installation directe sur Windows 10**  
[Microsoft Windows 10 Pro or Entreprise 64Bit](https://store.docker.com/editions/community/docker-ce-desktop-windows)

**Installation directe sur Windows 2016**  
[Installation sous Windows 2016](https://blogs.technet.microsoft.com/canitpro/2016/10/26/step-by-step-setup-docker-on-your-windows-2016-server/)

**Mac OS**  
[Apple MAC OS Yosemite 10.10.3 et suivants](https://store.docker.com/editions/community/docker-ce-desktop-mac)

**Linux**  
[Suivant la distribution](https://store.docker.com/search?offering=community&operating_system=linux&q=&type=edition)

**Versions précédentes de Windows et Mac**  
[Docker Toolbox](https://www.docker.com/products/docker-toolbox)

**Installation dans une machine virtuelle Linux via VirtualBox**  
A disposition 2 vms XUbuntu 17.10 64bits  
[XUbuntu Docker](https://fichiers.krementlibre.org/index.php/s/CW3lbj6C7Lmmz7E)

login : workshop-docker  
password : workshopdocker 


## Tester l'installation

La première commande pour tester si l'installation de Docker est fonctionnelle :  

    # docker container run hello-world
 

Cette commande va simplement afficher un message du genre :

    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    5b0f327be733: Pull complete 
    Digest: sha256:07d5f7800dfe37b8c2196c7b1c524c33808ce2e0f74e7aa00e603295ca9a0972
    Status: Downloaded newer image for hello-world:latest
     
    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    To generate this message, Docker took the following steps:
     1. The Docker client contacted the Docker daemon.
     2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
     3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
     4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.
    To try something more ambitious, you can run an Ubuntu container with:
     $ docker run -it ubuntu bash
      
    Share images, automate workflows, and more with a free Docker ID:
     https://cloud.docker.com/
    For more examples and ideas, visit:
     https://docs.docker.com/engine/userguide/

## A vous de jouer

Installez et validez l'installation de Docker sur votre machine ou sur une machine virtuelle mise en place via VirtualBox.