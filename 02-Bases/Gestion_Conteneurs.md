# Gestion des conteneurs

La commande de gestion des conteneurs est la commande 
    
    # docker container 

Cette commande dispose d'un sous ensemble de commandes et d'options. Nous allons en voir certaines :

	Commands:
	  attach      Attach local standard input, output, and error streams to a running container
	  commit      Create a new image from a container's changes
	  cp          Copy files/folders between a container and the local filesystem
	  create      Create a new container
	  diff        Inspect changes to files or directories on a container's filesystem
	  exec        Run a command in a running container
	  export      Export a container's filesystem as a tar archive
	  inspect     Display detailed information on one or more containers
	  kill        Kill one or more running containers
	  logs        Fetch the logs of a container
	  ls          List containers
	  pause       Pause all processes within one or more containers
	  port        List port mappings or a specific mapping for the container
	  prune       Remove all stopped containers
	  rename      Rename a container
	  restart     Restart one or more containers
	  rm          Remove one or more containers
	  run         Run a command in a new container
	  start       Start one or more stopped containers
	  stats       Display a live stream of container(s) resource usage statistics
	  stop        Stop one or more running containers
	  top         Display the running processes of a container
	  unpause     Unpause all processes within one or more containers
	  update      Update configuration of one or more containers
	  wait        Block until one or more containers stop, then print their exit codes

## docker container run

Commande principale qui permet d'instancier un conteneur. Par exemple nous allons ici instancier un conteneur alpine linux et faire exécuter une commande dans ce conteneur :

	# docker container run alpine echo 'hello world of docker'
	Unable to find image 'alpine:latest' locally
	latest: Pulling from library/alpine
	b56ae66c2937: Pull complete 
	Digest: sha256:b40e202395eaec699f2d0c5e01e6d6cb8e6b57d77c0e0221600cf0b5940cf3ab
	Status: Downloaded newer image for alpine:latest
	hello world of docker

La syntaxe est la suivante : 
*docker container run [options] [image] [commande]*

## docker container ps

Cette commande permet de lister les conteneurs actifs

	# docker container ps
	CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

*WTF ??????* aucun conteneur n'est actif ... il sont ou mes conteneurs hello-world et alpine ?

En fait ces deux conteneurs ont été instanciés, ils ont fait le job et donc ce sont ensuite arrêtés.

Pour lister tout les conteneurs, même ceux arrêtés :

	# docker container ps -a
	CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
	a6704006280d        alpine              "echo 'hello world..."   3 minutes ago       Exited (0) 3 minutes ago                        zealous_chandrasekhar
	f70e1620ae9b        hello-world         "/hello"                 29 minutes ago      Exited (0) 29 minutes ago                       thirsty_colden

 Ceux deux affichages nous permettent de voir les infos suivantes :
 * CONTAINER ID : l'identifiant unique du conteneur
 * IMAGE : l'image utilisée
 * COMMAND : la commande exécutée au démarrage du conteneur
 * CREATE : depuis quand à été isntancié le conteneur
 * STATUS : le status du conteneur
 * PORTS : on en reparle plus tard
 * NAMES : le nom du conteneur

## Nommer un conteneur et se connecter dedans

Premier problème, le nom du conteneur, si on ne le précise pas il est généré automatiquement avec deux mots séparés par un underscore comme  zealous_chandrasekhar. Pas terrible.

Second problème je voudrais pouvoir me connecter dans le conteneur et y lancer des commandes.

Voici comment faire :

    # docker container run --name cont01 --hostname cont01 -it alpine
    / # 

Première remarque, la création du conteneur est presque instantannée, l'image est déjà en place.

Seconde remarque, notre prompt signale qu'est maintenant "dans" le conteneur.

Tapez les commandes que vous voulez et sortez avec *exit*.

Est-ce que le conteneur est encore actif ?

Non ... la commande exit à terminé la commande de démarrage du conteneur et donc arrêté le conteneur.


## Démarrer un conteneur en arrière plan

Pour qu'un conteneur reste actif il faut deux choses :
* qu'il soit démarré avec une commande exécutant un service ou une application
* le démarrer en mode démon

Par exemple on va démarrer un conteneur faisant fonctionner un serveur Web :

	# docker container run --name web01 --hostname web01 --restart always -d nginx
	Unable to find image 'nginx:latest' locally
	latest: Pulling from library/nginx
	bc95e04b23c0: Pull complete 
	aee0c172e58a: Pull complete 
	c2a5d8ccfabc: Pull complete 
	Digest: sha256:adea4f68096fded167603ba6663ed615a80e090da68eb3c9e2508c15c8368401
	Status: Downloaded newer image for nginx:latest
	8c16655a0564c25ef3e8079d00dab34356d0f548c433d5aa242642ce9ffb6fa9

C'est l'option *-d* qui nous permet de démarrer le conteneur en arrière plan.

On liste les conteneurs actifs :

	# docker container ls 
	CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS               NAMES
	8c16655a0564        nginx               "nginx -g 'daemon ..."   About a minute ago   Up About a minute   80/tcp              web01

Cette fois-ci on a bien un conteneur actif et on remarque une information supplémentaire dans la colonne PORTS, on y reviens plus tard.

## Entrer dans un conteneur

La commande référence pour connecter le terminal courant au terminal du conteneur est **docker container attach**, le problème c'est que cette commande ne produit souvent pas d'effet.

Le plus simple pour se connecter dans un conteneur et d'y faire exécuter un shell sh ou bash :

    # docker container exec -it web01 /bin/bash
    root@web01:/# 

Vous pouvez alors y lancer toutes les commandes disponibles dans le conteneur et naviguer dans les répetoires.

Pour sortir exit permettra de terminer l'exécution du shell bash sans arrêter le fonctionnement du conteneur.

De la même façon la commande exec permet de faire exécuter des commandes dans le conteneur :

	# docker container exec -it web01 ls -la
	total 72
	drwxr-xr-x   1 root root 4096 Nov  3 07:44 .
	drwxr-xr-x   1 root root 4096 Nov  3 07:44 ..
	-rwxr-xr-x   1 root root    0 Nov  3 07:44 .dockerenv
	drwxr-xr-x   2 root root 4096 Oct  9 00:00 bin
	drwxr-xr-x   2 root root 4096 Jul 13 13:04 boot
	drwxr-xr-x   5 root root  340 Nov  3 07:46 dev
	drwxr-xr-x   1 root root 4096 Nov  3 07:44 etc
	drwxr-xr-x   2 root root 4096 Jul 13 13:04 home
	drwxr-xr-x   1 root root 4096 Oct  9 00:00 lib
	drwxr-xr-x   2 root root 4096 Oct  9 00:00 lib64
	drwxr-xr-x   2 root root 4096 Oct  9 00:00 media
	drwxr-xr-x   2 root root 4096 Oct  9 00:00 mnt
	drwxr-xr-x   2 root root 4096 Oct  9 00:00 opt
	dr-xr-xr-x 221 root root    0 Nov  3 07:46 proc
	drwx------   1 root root 4096 Nov  3 07:49 root
	drwxr-xr-x   1 root root 4096 Nov  3 07:46 run
	drwxr-xr-x   2 root root 4096 Oct  9 00:00 sbin
	drwxr-xr-x   2 root root 4096 Oct  9 00:00 srv
	dr-xr-xr-x  13 root root    0 Nov  3 07:46 sys
	drwxrwxrwt   1 root root 4096 Oct 27 22:18 tmp
	drwxr-xr-x   1 root root 4096 Oct  9 00:00 usr
	drwxr-xr-x   1 root root 4096 Oct  9 00:00 var




## stop/start/pause/unpause

Les sous-commandes parlent d'elles-mêmes.

    # docker container stop web01
    # docker container start web01
    # docker container pause web01
    # docker container unpause web01

## Supprimer un conteneur

Attention un conteneur ne peut-être supprimé que si il est arrêté :

    # docker container rm hello-world

Il existe quand même une possibilité de supprimer un conteneur qui n'est pas arrêté :

    # docker container rm -f web01

Petite astuce pour faire du ménage et supprimer tout les conteneurs :

    # docker container rm $(docker container ls -aq)

## Tout tout tout je vous dirais tout sur ...

... mais non sur les conteneurs ...

On a une commande qui va nous permettre de connaître toutes les infos sur un conteneur :

    # docker container inspect web01

Ok cool mais il y en a un peu trop.

On à la possibilité de trier un peu les infos à afficher :

	# docker container inspect --format='{{json .NetworkSettings}}' web01
	{"Bridge":"","SandboxID":"530c3137089dbd01afb9f448233a9ca55a06ebcde4aed9e0eba3131ca6d6b9db","HairpinMode":false,"LinkLocalIPv6Address":"","LinkLocalIPv6PrefixLen":0,"Ports":{"80/tcp":null},"SandboxKey":"/var/run/docker/netns/530c3137089d","SecondaryIPAddresses":null,"SecondaryIPv6Addresses":null,"EndpointID":"ebdc7967015404b01cc097209aae167bd707a9a49ea4e298480b88fbcccff6c9","Gateway":"172.17.0.1","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","MacAddress":"02:42:ac:11:00:02","Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"32f921ac922aab7bdf4098fe500711e5047230207bde2e61d5a2d49651292c9f","EndpointID":"ebdc7967015404b01cc097209aae167bd707a9a49ea4e298480b88fbcccff6c9","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}}

	$ docker container inspect --format='{{.NetworkSettings.IPAddress}}' web01
	172.17.0.2

Il existe d'autres formats disponibles pour l'affichage.

## Les logs

Chaque conteneur produit ses logs, il est essentiel pour débugguer de savoir comment les lire.

    # docker container logs web01

Pour suivre en live les logs :

    # docker container logs -f web01

Notre conteneur web01 ne fait pas grand chose ... mais gardez cette commande en tête en cas de besoins.
