# Les bases

Maintenant que Docker est installé et fonctionnel nous allons voir les commandes de bases.

## Les concepts essentiels de Docker

### Démon et Client


_Le Démon_
* dockerd
* gère : les images, les réseaux, les volumes, les clusters
* délègue la gestion des conteneurs à containerd
* expose une API en HTTP Rest
* écoute sur un socker unix et/ou tcp

_Le Client_
* installé avec le démon
* communique avec lui au travers du socker unix
* peut communiquer avec des démons distants via tcp

Le client est la ligne de commande qui nous allons utiliser pour faire fonctionner les différents composants de Docker.

### Les images

Il faut voir une image un template qui va servir à l'instanciation de conteneur, sans images pas de conteneurs.

### Les registres

Les registres sont les dépôts sur lesquels on va pouvoir récupérer les images, voir en stocker sous certaines conditions.

Le dépôt utilisé par défaut est le [hub docker](https://hub.docker.com/) qui petit à petit est transféré vers le [store docker](https://store.docker.com/).
Nous allons utiliser principalement ce dépôt.

Il faut savoir qu'il est possible, pour avoir un registre privé, d'héberger son propre registre, voir de le déléguer pour des fournisseurs. Par exemple [OVH Docker Registry](https://labs.ovh.com/ovh-docker-registry)

### Un schéma de l'ensemble

![Docker Architecture](../images/docker_architecture.png)

