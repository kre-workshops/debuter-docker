# Ports réseau

Petit rappel si je regarde le conteneur web01 :

	# docker container ls 
	CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
	95d9b2a66909        nginx               "nginx -g 'daemon ..."   About an hour ago   Up About an hour    80/tcp              web01

La première idée est donc de récupérer l'IP de la machine faisant tourner Docker et avec un navigateur d'ouvrir le port 80 pour voir la super page d'accueil de Nginx ... fail ...

WTF ?????

Le port réseau ouvert par se conteneur est ouvert sur l'adresse IP du conteneur.  

Quand Docker démarre il crée un sous réseau et affecte une adresse IP à chaque conteneur sur ce sous-réseau. 

Rappel pour récupérer l'IP du conteneur :

    # docker container inspect --format='{{.NetworkSettings.IPAddress}}' web01
    172.17.0.2

Suivant la configuration de la machine hébergeant Docker il va falloir faire différentes confirations réseau pour atteindre ce réseau. C'est en dehors du sujet de ce workshop. Juste un exemple pour ceux utilisant shorewall (gestion des iptables) : [Docker et Shorewal](https://www.hisyl.fr/docker-et-shorewall/)

Ce qui nous intéresse est quand même de pouvoir accéder à notre conteneur suivant le service qu'il propose.

On va donc binder les ports de la machine locale sur les ports du conteneur.

Supprimez le conteneur web01.

## Démarrer et utiliser ghost

On va jouer avec les ports avec le conteneur ghost.  
Ghost est par défaut disponible sur le port 2368/tcp on veut pouvoir y accéder via le port 80 :

    # docker container run --name ghost01 -d -p 80:2368 ghost

La configuration des binding de ports va se faire via l'option -p à gauche le port local, à droite le port sur le conteneur.

Si on utilise le navigateur on va donc pouvoir accéder à la partie web de notre conteneur.

On peut bien sur aussi binder plusieurs ports :

    # docker container run --name mat01 -d -p 3306:3306 -p 8065:8065 mattermost/mattermost-preview

## Les réseaux

Il est possible de créer plusieurs sous-réseaux dans Docker, quand on met en place des clusters avec Swarm par exemple on va créer des réseaux entre chaque serveur Docker.

Pour lister les réseaux en place :

	$ docker network ls
	NETWORK ID          NAME                DRIVER              SCOPE
	1a0ba4432618        bridge              bridge              local
	bdf4f867214c        host                host                local
	056285b88244        none                null                local

On ira pas plus loin sur ce sujet dans ce workshop.

## Proxy

Pour mettre en place plusieurs conteneurs web sur le même serveur physique il faudra mettre en place un proxy. Souvent on retrouvera Nginx dans ce rôle.  
Il faut aussi que le proxy soit mis à jour automatiquement lors de l'instanciation d'un nouveau conteneur, il faut donc que le proxy soit connecté sur le socket de Docker pour écouter l'instanciation des nouveaux conteneur et mettre à jour le proxy.  

Un très bon projet pour ça [Traefik](https://traefik.io/). Si on a le temps je vous fait une démo.
