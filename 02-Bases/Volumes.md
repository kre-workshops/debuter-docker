# Les volumes

Le but d'un conteneur c'est le faire vivre et de permettre de faire évoluer l'application qu'il isole. Donc l'application va évoluer de versions en version et le conteneur va donc être remplacé par un nouveau conteneur avec la nouvelle version de l'application.  

Problème, si on stocke les données dans le conteneur, quand on va instancier un nouveau conteneur celles-ci vont être perdues. Que ce soit, les fichiers de configuration, les logs, les données de fonctionnement de l'application. 

Il faut donc isoler les données de l'application, on va ainsi pouvoir faire évoluer l'application sans perdre les données et les configurations.

C'est le rôle des **volumes**

De base si on ne précise rien à Docker, des volumes sont créés suivant la définition de l'image, ces volumes sont stockés dans **/var/lib/docker/volumes**.  
Ils sont visibles grâce à la commande :

	# docker volume ls
	DRIVER              VOLUME NAME
	local               10c847168d0a4631f5c0f42b7e52ccd3e9052e6d5ee51272c259ad93bdafc1ce
	local               5ffb66dc1de637425a967a92e86a46cffb6dc79c9fceb5a458c1dc509b469cee
	local               757dd97b4edb4103aa094ae8dc0cc8fd8bde85da839627f5fb9eaf53267d8be6
	local               913f505beffd61db26a4746b7bfb13e39f84dfd8997c8442b0d5197663fca001
	local               9a320edae2ae80566d5dd785d04053d9f3176254317a544b2e6e8c57654bfeca
	local               a99d1ce59e4d8b37e6e222129d08f0d3cb46266027c9644dc16e0574f70885a7
	local               c634110d689f1d2125de7d7652b1c6fd6944841c4817d4a3281806720c49ee86

Pour savoir quel conteneur à créé quel volume on a les infos grâce à la commande inspect :

	# docker container inspect -f '{{json .Mounts }}' ghost01 |python -m json.tool
	[
	    {
	        "Destination": "/var/lib/ghost/content",
	        "Driver": "local",
	        "Mode": "",
	        "Name": "10c847168d0a4631f5c0f42b7e52ccd3e9052e6d5ee51272c259ad93bdafc1ce",
	        "Propagation": "",
	        "RW": true,
	        "Source": "/var/lib/docker/volumes/10c847168d0a4631f5c0f42b7e52ccd3e9052e6d5ee51272c259ad93bdafc1ce/_data",
	        "Type": "volume"
	    }
	]


On sait donc quel répertoire est monté dans quel répertoire dans le conteneur. A savoir que si vous supprimez le conteneur le volume n'est pas supprimé sauf si on le précise à la création. On peut donc rapidement se retrouver avec des volumes inutilisés.

La commande suivante permet de lister les volumes inutilisés :

    # docker volume ls -qf dangling=true
    757dd97b4edb4103aa094ae8dc0cc8fd8bde85da839627f5fb9eaf53267d8be6
    913f505beffd61db26a4746b7bfb13e39f84dfd8997c8442b0d5197663fca001
    9a320edae2ae80566d5dd785d04053d9f3176254317a544b2e6e8c57654bfeca
    c634110d689f1d2125de7d7652b1c6fd6944841c4817d4a3281806720c49ee86


## Supprimer un volume

Pour supprimer un volume /!\ Les données sont supprimées cette fois-ci /!\

    # docker volume rm 757dd97b4edb4103aa094ae8dc0cc8fd8bde85da839627f5fb9eaf53267d8be6
    757dd97b4edb4103aa094ae8dc0cc8fd8bde85da839627f5fb9eaf53267d8be6

Pour supprimer tout les volumes inutilisés :

    # docker volume rm `docker volume ls -qf dangling=true`

## Binder un répertoire local

Pour de l'hébergement de sites web, par exemple il pourrait être utile d'avoir sur le système de fichier du serveur les données des sites et que le conteneur vienne y accéder.  
On à par exemple un répertoire **~/tmp/site1/** dans lequel on a le code html de notre site.

    # docker container run --name site01 -p 80:80 -v ~/tmp/site1:/usr/share/nginx/html -d nginx

On a accès directement aux fichiers html donc on peut les modifier en live.

Si je détruits le conteneur et que j'en recrée un autre :

    # docker container run --name site01-bis -p 80:80 -v  ~/tmp/site1:/var/www/html -d nimmis/apache-php5

On a donc créé une séparation entre les données et le côté fonctionnel de l'application, ici un serveur Web.

## Créer des volumes nommés

Il va être plus souvent utilisé la méthode suivante de créer des volumes nommés qui vont être mis sur le système de fichier standard de Docker /var/lib/docker/volumes mais qui par contre au lieux d'avoir un nom incompréhensible vont pouvoir avoir un nom dédié.

Dans un premier temps on crée le volume :

    # docker volume create --name ghost_data

	# docker volume ls
	DRIVER              VOLUME NAME
	local               ghost_data

On peut regarder toute ses infos avec inspect :

	# docker volume inspect ghost_data
	[
	    {
	        "CreatedAt": "2017-11-03T11:40:38+01:00",
	        "Driver": "local",
	        "Labels": {},
	        "Mountpoint": "/var/lib/docker/volumes/ghost_data/_data",
	        "Name": "ghost_data",
	        "Options": {},
	        "Scope": "local"
	    }
	]

Pour l'utiliser : 

    # docker container run --name ghost01 -p 80:2368 -v ghost_data:/var/lib/ghost/content -d ghost

Les données sont bien stockées dans le bon volume :

	# ls -la /var/lib/docker/volumes/ghost_data/_data
	total 28
	drwxr-xr-x 7 guidtz guidtz 4096 nov.   3 11:44 .
	drwxr-xr-x 3 root   root   4096 nov.   3 11:40 ..
	drwxr-xr-x 2 guidtz guidtz 4096 nov.   1 22:34 apps
	drwxr-xr-x 2 guidtz guidtz 4096 nov.   3 11:44 data
	drwxr-xr-x 2 guidtz guidtz 4096 nov.   1 22:34 images
	drwxr-xr-x 2 guidtz guidtz 4096 nov.   3 11:44 logs
	drwxr-xr-x 2 guidtz guidtz 4096 nov.   1 22:35 themes

On peut détruire et recréer notre conteneur ghost sans perdre nos données.

## Volumes externes

Il a différentes solutions pour stocker les volumes autrepart que sur le disque de la machine qui fait tourner Docker, c'est très utile quand on commence à monter des clusters.  
Il existe différentes solutions, que ce soit des solutions d'infra comme NFS, GlusterFS ou Ceph ou des solutions fournies par des [plugins spécifique](https://store.docker.com/search?category=volume&q=&type=plugin)
