# Gestion des images

Nous allons voir ici comment gérer les images. Pour rappel, tout conteneur doit être instancié à partir d'une image.  
Nous ne verrons pas sur ce workshop comment créer des images, on va utiliser les images du registre standard.  

## Chercher sur le hub docker

Le registre par défaut de Docker pour le stockage des images est [Hub Docker](https://hub.docker.com/).  
Il existe plus de 100 000 images sur ce dépôt.  
Le hub Docker est en train de migrer vers le [Docker Store](https://store.docker.com), mettant à disposition des images avec des licences non libres.  
Le premier réflexe est d'aller chercher avec un navigateur l'image qui nous intéresse. Pour l'exemple on va déployer un conteneur Ghost.  

On peut directement en console aller chercher les images dans le hub :

	# docker search ghost
	NAME                                         DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
	ghost                                        Ghost is a free and open source blogging p...   659                 [OK]                
	ptimof/ghost                                 Allows running Ghost in production mode. B...   18                                      [OK]
	zzrot/alpine-ghost                           Docker Image running Ghost Blogging Servic...   15                                      [OK]
	diancloud/ghost                              Ghost中文版-最好用的独立博客平台                             11                                      
	gold/ghost                                   Configurable Golden Ghost image.                9                                       [OK]
	alexellis2/ghost-on-docker                   Pair Docker and Ghost for the perfect plat...   9                                       
	bitnami/ghost                                Bitnami Docker Image for Ghost                  7                                       [OK]
	wonderfall/ghost                             Minimal Ghost CMS image ready for production.   6                                       [OK]
	kitematic/ghost                              Ghost is a simple publishing platform that...   5                                       [OK]
	kennethlimcp/armhf-ghost                     The awesome Ghost blog, on armhf                2                                       [OK]
	nlhkh/ghost                                  ghost in production                             2                                       [OK]
	sapk/ghost                                   A simple and light ghost instance               1                                       [OK]
	patrckbrs/ghost-rpi                          Ghost est une plateforme de publication po...   1                                       
	jonnybgod/ghost                              Docker Alpine Ghost image                       1                                       [OK]
	fixer/ghostfixer                             This is an unofficial build of the awesome...   1                                       [OK]
	mmornati/docker-ghostblog                    Create your personal Ghost blog using this...   1                                       [OK]
	thedigitalgarage/ghost-mysql                 Ghost blogging platform configured with a ...   0                                       [OK]
	amd64/ghost                                  Ghost is a free and open source blogging p...   0                                       
	trollin/ghost                                                                                0                                       
	thedigitalgarage/ghost                       This is ghost container for TheDigitalGarage.   0                                       [OK]
	abrown/ghost                                 Ghost blog with s3 module                       0                                       [OK]
	husseingalal/ghost                                                                           0                                       
	aarongorka/ghost                             Up to date Ghost (blogging platform) conta...   0                                       [OK]
	mmornati/docker-ghostblog-cloudinary         https://github.com/mmornati/docker-ghostbl...   0                                       
	dingotiles/ghost-for-cloudfoundry-pipeline                                                   0                                       

On y retrouve un système de classement avec les images officielles et un système de notation par étoiles.

Par contre il ne faut pas hésiter à aller voir les informations de chaque images pour voir comment elle est créée, par qui, dans quel contexte, etc ... 

## Les tags

Si on regarde les détails de l'image officielle ghost : [ghost](https://store.docker.com/images/ghost). On retrouve un certain nombre d'informations dont une série de versions : 1.16.1 0.11 etc ... Ces versions sont tagguées.  
Lors de la récupération d'une image, si on ne précise pas le tag, le tag par défaut sera latest.  
On va voir comment récupérer deux versions de la même image.  

## Récupérer une image en local

Le mécanisme du client Docker, lors de l'instanciation d'un conteneur est d'aller récupérer sur le hub les images si elles ne sont pas stockées en local :

	Unable to find image 'nginx:latest' locally
		latest: Pulling from library/nginx

On peut aussi récupérer manuellement une image pour l'utiliser plus tard :

	$ docker image pull ghost
	Using default tag: latest
	latest: Pulling from library/ghost
	85b1f47fba49: Pull complete 
	5409e9a7fa9e: Pull complete 
	e1fbf5f3bc23: Pull complete 
	01c6a2f10685: Pull complete 
	d0681cbb7979: Pull complete 
	150f92d22996: Pull complete 
	9aaa9d3bc21d: Pull complete 
	711990f69101: Pull complete 
	162ef96752c3: Pull complete 
	142a2d607944: Pull complete 
	Digest: sha256:6928eb1a8e022702377b181d96c0717c6c80876f617a4cd49d0acdb63c464801
	Status: Downloaded newer image for ghost:latest

Si on veut récupérer une autre version de l'image :

	$ docker image pull ghost:0.11
	0.11: Pulling from library/ghost
	85b1f47fba49: Already exists 
	5409e9a7fa9e: Already exists 
	e1fbf5f3bc23: Already exists 
	01c6a2f10685: Already exists 
	d0681cbb7979: Already exists 
	150f92d22996: Already exists 
	420a6f8bc26d: Pull complete 
	84e82c50cf87: Pull complete 
	3a563a5e799a: Pull complete 
	050cb2805109: Pull complete 
	651bc9ef2f90: Pull complete 
	85b49049d2c3: Pull complete 
	6c9f2ae47842: Pull complete 
	Digest: sha256:5324f34543ad8bdfb33e97093d46f6a04c326b74cfe16d66c902372e2b400bc0
	Status: Downloaded newer image for ghost:0.11

A partir de maintenant si on veut déployer un conteneur ghost, l'image étant en local, le conteneur sera déployé rapidement. On utilisera ghost dans le chapitre suivant sur la gestion des ports réseau.

## Lister les images présentes

La commande de gestion des images est **docker image**

Pour lister les images présentes en local sur la machine :

	# docker image ls
	REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
	ghost               0.11                6a576b4a2eeb        35 hours ago        330MB
	ghost               latest              2f9b137d778e        35 hours ago        621MB
	nginx               latest              c59f17fe53b0        6 days ago          108MB
	alpine              latest              37eec16f1872        8 days ago          3.97MB
	hello-world         latest              05a3bd381fc2        7 weeks ago         1.84kB

## Inspecter une image

Comme pour les conteneur il est possible de tout savoir sur une image :

    # docker image inspect ghost:0.11

## Supprimer une image

Si une image n'est plus nécessaire, il est intéressant de la supprimer pour récupérer de l'espace disque.

	# docker image rm hello-world
	Untagged: hello-world:latest
	Untagged: hello-world@sha256:07d5f7800dfe37b8c2196c7b1c524c33808ce2e0f74e7aa00e603295ca9a0972
	Deleted: sha256:05a3bd381fc2470695a35f230afefd7bf978b566253199c4ae5cc96fafa29b37
	Deleted: sha256:3a36971a9f14df69f90891bf24dc2b9ed9c2d20959b624eab41bbf126272a023

Attention bien sur on ne pourra pas supprimer une image étant utilisée par un conteneur :

	# docker image rm nginx
	Error response from daemon: conflict: unable to remove repository reference "nginx" (must force) - container 95d9b2a66909 is using its referenced image c59f17fe53b0
