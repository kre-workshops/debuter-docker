# Les interfaces graphiques

Il existe plusieurs projets plus ou moins aboutis de gestion des conteneurs via des interfaces.

***ATTENTION*** ces interfaces ne font pas office d'orchestrateurs pour gérer des clusters complet de serveurs, sauf un peu pour portainer.

## Kitematic

![Kitematic](../images/kitematic.png)

Outils dédié pour les environnements Windows et Mac.  
Kitematic est intégré dans les Docker Toolbox ou peut être [téléchargé](https://github.com/docker/kitematic/releases)

Permet de :
* déployer des conteneurs  
* les configurer  
* accéder au docker hub  
* avoir une console sur les conteneurs
* voir les logs

[Site](https://kitematic.com/)

## Portainer

Portainer va permettre de gérer autant de choses que Kitematic mais va aussi permettre de gérer :
* les réseaux
* les clusters swarm
* gérer plusieurs serveurs Dockers

Portainer se déploie sous forme de conteneur :

    # docker volume create portainer_data
    # docker run --name portainer -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

Portainer est maintenant disponible sur le port 9000 de la machine locale et écoute le sockt local Docker pour gérer les conteneurs locaux.  
A la première connexion créer le mot de passe administrateur.

Demo Portaine gérant le docker local et deux docker déportés sur vms

## Autres

[Comparatif Portainer et autres](https://portainer.io/portainer-comparison.html)
